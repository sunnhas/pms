<?php

namespace Controllers;

class Login extends Controller
{
	public function __invoke($req, $res)
	{
		if ($this->user) {
			return $this->redirect('/complaints');
		}
		
		$this->data['user'] = $_SESSION['user'];
		$this->render($res, 'login.twig');
		return $res;
	}
	
	public function login($req, $res)
	{
		$body = json_decode($req->getBody());
		
		$user = $this->post("/auth/login", [
			'username' => $body->username,
			'password' => $body->password
		]);
		
		if ($user != null && !$user->error)
		{
			$_SESSION['user'] = [
				'token' => $user->token,
				'id' => $user->user->id,
				'username' => $user->user->username
			];
			
			return $res->withJson($user->user, 200);
		}
		else
		{
			return $res->withJson([
				'error' => 'wrong username or password'
			], 401);
		}
	}
	
	public function logout($req, $res)
	{
		session_destroy();
		return $this->redirect('/complain');
	}
}