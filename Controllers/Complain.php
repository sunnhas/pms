<?php

namespace Controllers;

class Complain extends Controller
{
	public function __invoke($req, $res)
	{
		$this->data['categories'] = $this->get("/categories");
		
		$this->render($res, "complain.twig");
		return $res;
	}
}