<?php

namespace Controllers;

use Slim\Http\Response;

abstract class Controller
{
	protected $c;
	protected $data = [];
	protected $view;
	
	protected $user = null;
	
	private $backend_url;
	
	public function __construct($c)
	{
		$this->c = $c;
		$this->view = $this->c->view;
		$this->backend_url = $_SERVER['HTTP_HOST']."/api";
		
		if (isset($_SESSION['user']))
		{
			$this->data['user'] = $_SESSION['user'];
			$this->user = $_SESSION['user'];
		}
	}
	
	protected function render(Response $res, $template)
	{
		$this->view->render($res, $template, $this->data);
	}
	
	protected function redirect($path, $status = 303)
	{
		return $this->c->response
			->withStatus($status)
			->withHeader('Location', $path);
	}
	
	protected function get($url)
	{
		$curl = curl_init();
		curl_setopt_array($curl, [
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $this->backend_url . $url
		]);
		
		if ($this->user)
		{
			curl_setopt($curl, CURLOPT_HTTPHEADER, array(
				'X-Authorization: ' . $this->user['token']
			));
		}
		
		$result = curl_exec($curl);
		
		curl_close($curl);
		
		return json_decode($result);
	}
	
	protected function post($url, $data)
	{
		$curl = curl_init();
		curl_setopt_array($curl, [
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $this->backend_url . $url,
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => $data
		]);
		
		if ($this->user)
		{
			curl_setopt($curl, CURLOPT_HTTPHEADER, array(
				'Authorization: ' . $this->user['token']
			));
		}
		
		$result = curl_exec($curl);
		
		curl_close($curl);
		
		return json_decode($result);
	}
}