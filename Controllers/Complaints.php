<?php

namespace Controllers;

class Complaints extends Controller
{
	public function __invoke($req, $res)
	{
		if (!$this->user) {
			return $this->redirect('/login', 401);
		}
		
		$this->data['complaints'] = $this->get("/complaints");
		
		$this->render($res, "complaints.twig");
		return $res;
	}
	
	public function single($req, $res, $args)
	{
		if (!$this->user) {
			return $this->redirect('/login', 401);
		}
		
		$this->data['complaint'] = $this->get("/complaints/" . $args['id']);
		
		if ($this->data['complaint'] == null || $this->data['complaint']->error)
		{
			return $this->c['notFoundHandler']($req, $res);
		}
		
		$this->render($res, "complaints_single.twig");
		return $res;
	}
}