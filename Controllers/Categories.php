<?php

namespace Controllers;

class Categories extends Controller
{
	public function __invoke($req, $res)
	{
		if (!$this->user) {
			return $this->redirect('/login', 401);
		}
		
		$this->data['categories'] = $this->get("/categories");
		
		$this->render($res, "categories.twig");
		return $res;
	}
}