<?php

namespace Controllers;

class Jobs extends Controller
{
	public function __invoke($req, $res)
	{
		if (!$this->user) {
			return $this->redirect('/login', 401);
		}
		
		$this->data['jobs'] = $this->get("/jobs");
		$this->data['contractors'] = $this->get("/contractors");
		
		$this->render($res, "jobs.twig");
		return $res;
	}
	
	public function single($req, $res, $args)
	{
		if (!$this->user) {
			return $this->redirect('/login', 401);
		}
		
		$this->data['job'] = $this->get("/jobs/" . $args['id']);
		
		if ($this->data['job'] == null || $this->data['job']->error)
		{
			return $this->c['notFoundHandler']($req, $res);
		}
		
		$this->render($res, "jobs_single.twig");
		return $res;
	}
}