<?php

namespace Controllers;

class Contractors extends Controller
{
	public function __invoke($req, $res)
	{
		$this->data['contractors'] = $this->get('/contractors');
		
		$this->render($res, 'contractors.twig');
		return $res;
	}
}