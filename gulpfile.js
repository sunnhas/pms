var gulp = require("gulp");
var sass = require("gulp-sass");

var sassPaths = [
	'bower_components/foundation-sites/scss'
];

gulp.task("sass", function() {
	return gulp.src('resources/scss/app.scss')
		.pipe(sass({
				includePaths: sassPaths
			})
			.on('error', sass.logError))
		.pipe(gulp.dest('assets/css'));
});

gulp.task('default', ['sass'], function() {
	gulp.watch(['scss/**/*.scss'], ['sass']);
});