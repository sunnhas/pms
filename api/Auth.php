<?php

require_once "Database.php";

class Auth
{
	private $whiteList;
	private $c;
	
	public function __construct() {
		//Define the urls that you want to exclude from Authentication, aka public urls     
		$this->whiteList = [
			'get' => ['categories', 'seed'],
			'post' => ['auth/login', 'complaints']
		];
	}
	
	/**
	 * Call
	 *
	 * @todo beautify this method ASAP!
	 *
	 */
	public function __invoke($request, $response, $next) {
		//Get the token sent from jquery
		$token = $request->getHeader('HTTP_X_AUTHORIZATION')[0];
		$url = $request->getUri()->getPath();
		
		if (!$this->isPublicUrl($url, $request->getMethod()) && !$this->authenticate($token)) {
			return $this->deny_access($response);
		}
		
		// Update expire time on token here
		$this->updateToken($token);
		
		$next($request, $response);
		return $response;
	}
	
	/**
	 * Deny Access
	 *
	 */
	public function deny_access($res) {
		return $res->withJson([
			'error' => 'not authorized'
		], 401);
	}
	
	/**
	 * Check against the DB if the token is valid
	 *
	 * @param string $token
	 * @return bool
	 */
	public function authenticate($token) {
		$db = Database::getInstance();
		
		$db->query("SELECT * FROM admins WHERE token = :token");
		$db->bind(':token', $token);
		
		$user = $db->single();
		
		if (!$user) return false;
		
		$now = new DateTime();
		$expire = new DateTime($user['token_expire']);
		return $now->getTimestamp() < $expire->getTimestamp();
	}
	
	/**
	 * This function will compare the provided url against the whitelist and
	 * return whether the $url is public or not
	 *
	 * @param string $url
	 * @param string $method
	 * @return bool
	 */
	public function isPublicUrl($url, $method) {
		$method = strtolower($method);
		if (!$this->whiteList[$method]) return false;
		return in_array($url, $this->whiteList[$method]);
	}
	
	public function updateToken($token)
	{
		$db = Database::getInstance();
		$expires = date('Y-m-d H:i:s', strtotime('+1 hour'));
		
		$db->query("UPDATE admins SET token_expire = :expire WHERE token = :token");
		$db->bind(':token', $token);
		$db->bind(':expire', $expires);
		$db->execute();
	}
}