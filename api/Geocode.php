<?php

class Geocode
{
	private static $url = "https://maps.googleapis.com/maps/api/geocode/json";
	private static $key = "AIzaSyARkUd_J9vFo1hLlQsqibQ4_zOwcsBWBwA";
	
	public static function address($address)
	{
		return self::get(self::$url . "?address=$address&key=" . self::$key);
	}
	
	public static function reverse($lat, $lng)
	{
		return self::get(self::$url . "?latlng=$lat,$lng&key=" . self::$key);
	}
	
	protected static function get($url)
	{
		$curl = curl_init();
		curl_setopt_array($curl, [
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_URL => $url
		]);
		
		$result = curl_exec($curl);
		
		curl_close($curl);
		
		return json_decode($result);
	}
}