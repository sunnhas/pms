/*** ADMINS ***/
# Retrieve all
SELECT adminid FROM admins;
# Login
SELECT adminid FROM admins WHERE adminid = :id AND password = :password;

/*** CONTRACTORS ***/
# Retrieve all
SELECT * FROM contractors;
# By id
SELECT * FROM contractors WHERE contractorid = :id;

/*** CATEGORIES ***/
# Retrieve all
SELECT * FROM categories;

/*** COMPLAINTS ***/
# Retrieve all
SELECT * FROM complaints;
# By id
SELECT * FROM complaints WHERE complaintid = :id;
# Get images by id
SELECT * FROM imagescomplaintsjobs
	JOIN images ON imagescomplaintsjobs.image = images.imageid
	WHERE imagescomplaintsjobs.complaint = :id;

/*** JOBS **/
# Retrieve all
SELECT j.jobid, j.description, j.lat, j.lng, j.startdate,
	j.enddate, j.deadline, c.name AS contractor
	FROM jobs j
	JOIN contractors c
		ON j.contractor = c.contractorid;
# By id
SELECT j.jobid, j.description, j.lat, j.lng, j.startdate,
	j.enddate, j.deadline, c.name AS contractor
	FROM jobs j
	JOIN contractors c
		ON j.contractor = c.contractorid
	WHERE j.jobid = :id;
# Get images by id
SELECT * FROM imagescomplaintsjobs
	JOIN images ON imagescomplaintsjobs.image = images.imageid
	WHERE imagescomplaintsjobs.job = :id;