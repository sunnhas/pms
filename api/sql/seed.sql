INSERT INTO admins (username, password) VALUES
	('admin', '$2y$10$.i9jWQ3/SC849ojXYYC5Huqg2BMqOAdAhqIf4AtqadtzSGYsAxqku');

INSERT INTO images (url) VALUES
	('/something/something.jpg');

INSERT INTO contractors (name) VALUES
	('Working company 1'),
	('Working company 2');

INSERT INTO categories (name) VALUES
	('Pothole'),
	('Shoulder');

INSERT INTO complaints (category, severity, description, lat, lng) VALUES
	(2, 2, 'The shoulder is broken', 21.280032, -157.823925),
	(2, 3, 'The shoulder is broken here too', 21.280032, -157.823925),
	(1, 1, 'Pothole in the middle of the road', 21.280032, -157.823925);

INSERT INTO jobs (description, lat, lng, startDate, deadline, contractor) VALUES
	('Repair a broken should', 21.280032, -157.823925, '2016-04-25 12:45:45', '2016-05-02 13:00:00', 1);

INSERT INTO imagesComplaintsJobs (image, job, complaint) VALUES
	(1, 1, 1),
	(1, 1, 1);