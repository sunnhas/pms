CREATE OR REPLACE VIEW view_complaints AS
	SELECT c.complaintId, c.severity, c.date,
		c.description, c.lat, c.lng, cat.name AS category
	FROM complaints c
		JOIN categories cat ON c.category = cat.categoryid;

CREATE OR REPLACE VIEW view_jobs AS
	SELECT j.jobId, j.description, j.startDate, j.deadline,
		j.endDate, j.lat, j.lng, c.name AS contractor
	FROM jobs j
		JOIN contractors c ON j.contractor = c.contractorid;