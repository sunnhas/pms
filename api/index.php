<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once '../vendor/autoload.php';
require_once 'config.php';
require_once 'Database.php';
require_once 'Geocode.php';
require_once 'Auth.php';

date_default_timezone_set('Pacific/Honolulu');

$db = Database::getInstance();
$app = new \Slim\App();

/**
 * Add middlewares
 */
$app->add(new Auth());


/**
 * Admin routes
 */
$app->post('/auth/login', function(Request $request, Response $response) use ($db) {
	$body = (object) $request->getParsedBody();
	
	$db->query("SELECT * FROM admins WHERE username = :username");
	$db->bind(':username', $body->username);
	
	$user = $db->single();
	
	if ($user && password_verify($body->password, $user['password']))
	{
		$token = bin2hex(openssl_random_pseudo_bytes(16));
		$exp_date = date('Y-m-d H:i:s', strtotime('+1 hour'));
		
		$db->query(
			"UPDATE admins SET token = :token, token_expire = :exp
			WHERE adminId = :id"
		);
		$db->bind(':token', $token);
		$db->bind(':exp', $exp_date);
		$db->bind(':id', $user['adminId']);
		
		try {
			$db->execute();
			
			$auth = [
				'token' => $token,
				'user' => [
					'id' => $user['adminId'],
					'username' => $body->username
				]
			];
			
			return $response->withJson($auth, 200);
		} catch (PDOException $e) {
			return $response->withJson([
				'error' => $e->getMessage(),
				'token' => $token,
				'len' => strlen($token)
			], 200);
		}
	}
	
	return $response->withJson([
		'error' => 'username or password is wrong'
	], 200);
});

$app->get('/admins', function (Request $request, Response $response) use ($db) {
	$db->query("SELECT adminId, username, token, token_expire FROM admins");
	$data = $db->resultset();
	
	$response = $response->withJson($data, 200);
	
	return $response;
});

$app->post('/admins', function (Request $request, Response $response) use ($db) {
	$body = json_decode($request->getBody());
	
	$db->query("INSERT INTO admins (username, password) VALUES (:uname, :pass)");
	$db->bind(':uname', $body->username);
	$db->bind(':pass', password_hash($body->password, PASSWORD_BCRYPT));
	
	try {
		$db->execute();
		
		return $response->withJson([
			'id' => $db->lastInsertId(),
			'username' => $body->username
		], 200);
	} catch(PDOException $e) {
		return $response->withJson([
			'error' => $e->getMessage()
		], 500);
	}
});


/**
 * Category routes
 */
$app->get('/categories', function (Request $request, Response $response) use ($db) {
	$db->query("SELECT * FROM categories");
	return $response->withJson($db->resultset(), 200);
});

$app->post('/categories', function (Request $request, Response $response) use ($db) {
	$body = json_decode($request->getBody());
	
	$db->query("INSERT INTO categories (name) VALUES (:name)");
	$db->bind(':name', $body->name);
	
	try {
		$db->execute();
		return $response->withJson([
			'id' => $db->lastInsertId(),
			'name' => $body->name
		], 200);
	} catch (PDOException $e) {
		return $response->withJson([
			'error' => $e->getMessage()
		], 500);
	}
});

$app->put('/categories/{id}', function (Request $request, Response $response, $args) use ($db) {
	$body = json_decode($request->getBody());
	
	$db->query("UPDATE categories SET name = :name WHERE categoryId = :id");
	$db->bind(':id', $args['id']);
	$db->bind(':name', $body->name);
	
	try {
		$db->execute();
		return $response->withJson([
			'id' => $db->lastInsertId()
		], 200);
	} catch (PDOException $e) {
		return $response->withJson([
			'error' => $e->getMessage()
		], 500);
	}
});

$app->delete('/categories/{id}', function (Request $request, Response $response, $args) use ($db) {
	$db->query("DELETE FROM categories WHERE categoryId = :id");
	$db->bind(':id', $args['id']);
	
	try {
		$db->execute();
		return $response->withJson([
			'id' => $db->lastInsertId()
		], 200);
	} catch (PDOException $e) {
		return $response->withJson([
			'error' => $e->getMessage()
		], 500);
	}
});

$app->get('/contractors', function (Request $request, Response $response) use ($db) {
	$db->query("SELECT * FROM contractors");
	return $response->withJson($db->resultset(), 200);
});


/**
 * Contractor routes
 */
$app->post('/contractors', function (Request $request, Response $response) use ($db) {
	$body = json_decode($request->getBody());
	
	$db->query("INSERT INTO contractors (name) VALUES (:name)");
	$db->bind(':name', $body->name);
	
	try {
		$db->execute();
		return $response->withJson([
			'id' => $db->lastInsertId(),
			'name' => $body->name
		], 200);
	} catch (PDOException $e) {
		return $response->withJson([
			'error' => $e->getMessage()
		], 500);
	}
});

$app->delete('/contractors/{id}', function (Request $request, Response $response, $args) use ($db) {
	$db->query("DELETE FROM contractors WHERE contractorId = :id");
	$db->bind(':id', $args['id']);
	
	try {
		$db->execute();
		return $response->withJson([
			'id' => $db->lastInsertId()
		], 200);
	} catch (PDOException $e) {
		return $response->withJson([
			'error' => $e->getMessage()
		], 500);
	}
});


/**
 * Complain routes
 */
$app->get('/complaints', function (Request $request, Response $response) use ($db) {
	$db->query("SELECT * FROM view_complaints");
	$data = $db->resultset();
	
	return $response->withJson($data, 200);
});

$app->get('/complaints/{id}', function (Request $request, Response $response, $args) use ($db) {
	// Retrieve the complaint
	$db->query("SELECT * FROM view_complaints WHERE complaintId = :id");
	$db->bind(':id', $args['id']);
	$data = $db->single();
	
	if ($data)
	{
		// Retrieve it's images
		$db->query(
			"SELECT * FROM imagesComplaintsJobs
			JOIN images ON imagesComplaintsJobs.image = images.imageId
			WHERE imagesComplaintsJobs.complaint = :id"
		);
		$db->bind(':id', $args['id']);
		
		$data['images'] = $db->resultset();
		$data['road_info'] = Geocode::reverse($data['lat'], $data['lng'])->results[0]->formatted_address;
		
		$response = $response->withJson($data, 200);
	}
	else
	{
		$response = $response->withJson([
			"error" => "Complaint not found"
		], 404);
	}
	
	return $response;
});

$app->post('/complaints', function(Request $request, Response $response) use ($db) {
	$body = (object) $request->getParsedBody();
	$files = (object) $request->getUploadedFiles();
	
	// Save images
	$image_ids = [];
	if ($files->images)
	{
		foreach ($files->images as $file)
		{
			$folder = __DIR__ . "/uploads/";
			
			if (!file_exists($folder))
			{
				mkdir($folder);
			}
			
			$ext = explode('.', $file->getClientFilename());
			$ext = $ext[sizeof($ext) - 1];
			$name = date('Y-m-d') . '-' . generateRandomString() . '.' . $ext;
			
			try {
				$file->moveTo($folder . $name);
				
				$db->query("INSERT INTO images (url) VALUES (:url)");
				$db->bind(':url', "/api/uploads/$name");
				
				try {
					$db->execute();
					array_push($image_ids, $db->lastInsertId());
				} catch (PDOException $e) {}
			} catch (Exception $e) {}
		}
	}
	
	$db->query(
		"INSERT INTO complaints (category, severity, description, lat, lng)
 		VALUES (:cat, :sev, :des, :lat, :lng)"
	);
	
	$db->bind(':cat', $body->category);
	$db->bind(':sev', $body->severity);
	$db->bind(':des', $body->description);
	$db->bind(':lat', $body->lat);
	$db->bind(':lng', $body->lng);
	
	try {
		$db->execute();
		$id = $db->lastInsertId();
		
		if (sizeof($image_ids) > 0)
		{
			foreach ($image_ids as $img)
			{
				$db->query("INSERT INTO imagesComplaintsJobs (image, complaint) VALUES ($img, $id)");
				$db->execute();
			}
		}
		
		return $response->withJson([
			'id' => $db->lastInsertId(),
			'body' => $body,
			'files' => $files,
			'oldFiles' => $_FILES
		], 200);
	} catch (PDOException $e) {}
	
	return $response->withJson([
		"error" => "Something went wrong"
	], 500);
});

$app->delete('/complaints/{id}', function (Request $request, Response $response, $args) use ($db) {
	$db->query("DELETE FROM complaints WHERE complaintId = :id");
	$db->bind(':id', $args['id']);
	
	try {
		$db->execute();
		return $response->withJson([
			'id' => $db->lastInsertId()
		], 200);
	} catch (PDOException $e) {
		return $response->withJson([
			'error' => $e->getMessage()
		], 500);
	}
});


/**
 * Job routes
 */
$app->get('/jobs', function(Request $request, Response $response) use ($db) {
	$db->query("SELECT * FROM view_jobs");
	return $response->withJson($db->resultset(), 200);
});

$app->get('/jobs/{id}', function(Request $request, Response $response, $args) use ($db) {
	$db->query("SELECT * FROM view_jobs WHERE jobId = :id");
	$db->bind(':id', $args['id']);
	
	try {
		$data = $db->single();
		
		if ($data)
		{
			// Retrieve it's images
			$db->query(
				"SELECT * FROM imagesComplaintsJobs
			JOIN images ON imagesComplaintsJobs.image = images.imageId
			WHERE imagesComplaintsJobs.job = :id"
			);
			$db->bind(':id', $args['id']);
			
			$data['images'] = $db->resultset();
			
			$data['road_info'] = Geocode::reverse($data['lat'], $data['lng'])->results[0]->formatted_address;
			
			return $response->withJson($data, 200);
		}
		
		return $response->withJson([
			'error' => 'not found'
		], 404);
	} catch (PDOException $e) {
		return $response->withJson([
			'error' => $e->getMessage()
		], 500);
	}
});

$app->post('/jobs', function(Request $request, Response $response) use ($db) {
	$body = (object) $request->getParsedBody();
	
	$db->query(
		"INSERT INTO jobs (description, lat, lng,
			startDate, deadline, contractor)
		VALUES (:des, :lat, :lng, :sDate, :deadline, :con)"
	);
	$db->bind(':des', $body->description);
	$db->bind(':lat', $body->lat);
	$db->bind(':lng', $body->lng);
	$db->bind(':sDate', $body->startdate);
	$db->bind(':deadline', $body->deadline);
	$db->bind(':con', $body->contractor);
	
	try {
		$db->execute();
		$body->id = $db->lastInsertId();
		
		$db->query("SELECT * FROM contractors WHERE contractorId = " . $body->contractor);
		$body->contractor = $db->single()['name'];
		
		return $response->withJson($body, 200);
	} catch (PDOException $e) {
		return $response->withJson([
			'error' => $e->getMessage(),
			'body' => $body
		], 500);
	}
});

$app->delete('/jobs/{id}', function (Request $request, Response $response, $args) use ($db) {
	$db->query("DELETE FROM jobs WHERE jobId = :id");
	$db->bind(':id', $args['id']);
	
	try {
		$db->execute();
		return $response->withJson([
			'id' => $db->lastInsertId()
		], 200);
	} catch (PDOException $e) {
		return $response->withJson([
			'error' => $e->getMessage()
		], 500);
	}
});


/**
 * Seed route
 */
$app->get('/seed', function(Request $request, Response $response) use ($db) {
	// Recreate tables
	$db->query(file_get_contents("sql/create.sql"));
	try
	{
		$db->execute();
	}
	catch (PDOException $e)
	{
		return $response->getBody()->write($e->getMessage());
	}
	
	// Create views
	$db->query(file_get_contents("sql/views.sql"));
	try
	{
		$db->execute();
	}
	catch (PDOException $e)
	{
		return $response->getBody()->write($e->getMessage());
	}
	
	// Seed the database
	$db->query(file_get_contents("sql/seed.sql"));
	try
	{
		$db->execute();
	}
	catch (PDOException $e)
	{
		return $response->getBody()->write($e->getMessage());
	}
	
	return $response->getBody()->write("It worked!! :D");
});

$app->run();

function generateRandomString($length = 10) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}
