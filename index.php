<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once 'vendor/autoload.php';

session_start();

$app = new \Slim\App();

$c = $app->getContainer();

$c['view'] = function ($c) {
	$view = new \Slim\Views\Twig("Views", [
		'cache' => false
	]);
	
	$view->addExtension(new \Slim\Views\TwigExtension(
		$c['router'],
		$c['request']->getUri()
	));
	
	return $view;
};

$c['notFoundHandler'] = function ($c) {
	return new Controllers\NotFoundHandler($c->get('view'), function ($request, $response) use ($c) {
		return $c['response']
			->withStatus(404);
	});
};

$app->get('/', function (Request $request, Response $response) {
	return $response->getBody()->write("Hello, this is a test page");
});

$app->get('/login', 'Controllers\Login');
$app->post('/login', 'Controllers\Login:login');
$app->get('/logout', 'Controllers\Login:logout');
$app->get('/complain', 'Controllers\Complain');
$app->get('/categories', 'Controllers\Categories');
$app->get('/contractors', 'Controllers\Contractors');
$app->get('/complaints', 'Controllers\Complaints');
$app->get('/complaints/{id}', 'Controllers\Complaints:single');
$app->get('/jobs', 'Controllers\Jobs');
$app->get('/jobs/{id}', 'Controllers\Jobs:single');

$app->run();